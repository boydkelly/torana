# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2020-12-07 14:53+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: content/en/post/demo.adoc:1 content/en/post/asciidoc-quick-reference.adoc:1
#: content/en/post/asciidoc-quick-reference.adoc:2427
#: content/en/post/asciidoc-quick-reference.adoc:2438
#: content/en/post/smoketest.adoc:1 content/en/notification/01-mediaplus.adoc:1
#: content/en/notification/ankataa.com.adoc:1
#: content/en/notification/bebiphilip.adoc:1
#: content/en/notification/bible.adoc:1
#: content/en/notification/mandenkan.com.adoc:1
#: content/en/notification/rfi.adoc:1 content/en/notification/jw.org.adoc:1
#: content/en/notification/fonts_google.adoc:1
#: content/en/notification/fonts_ngalonci.adoc:1
#: content/en/page/linkedin.adoc:1 content/en/page/contact.adoc:1
#: content/en/page/home.adoc:1 content/en/about/index.adoc:1
#, no-wrap
msgid "---\n"
msgstr ""

#. type: Attribute :lang:
#: content/en/notification/ankataa.com.adoc:20
#: content/en/notification/bible.adoc:21
#: content/en/notification/mandenkan.com.adoc:20
#: content/en/notification/rfi.adoc:20 content/en/notification/jw.org.adoc:21
#: content/en/notification/fonts_google.adoc:20
#: content/en/notification/fonts_ngalonci.adoc:20
#, no-wrap
msgid "en "
msgstr ""

#. type: Plain text
#: content/en/notification/bible.adoc:10
#, no-wrap
msgid ""
"title: The Bible\n"
"date:  2020-05-08T14:07:30\n"
"type: notification\n"
"draft: false \n"
"tags: [ \"jula\", \"language\"]\n"
"image: /images/bibleci.png\n"
"categories: [\"Julakan\" ]\n"
"hyperlink: \"https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US\"\n"
"---\n"
msgstr ""

#. type: Title =
#: content/en/notification/bible.adoc:12
#, no-wrap
msgid "The Bible"
msgstr ""

#. type: Table
#: content/en/notification/bible.adoc:30
#, no-wrap
msgid ""
"2+|Compare the text of the Bible in Ivory Coast and Burkina Faso Jula with the French Louis Ségon version.\n"
"|link:https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible[Bible en Dioula de Côte d'Ivoire]\n"
"|link:https://play.google.com/store/apps/details?id=com.dioula.parolededieu.burkinafaso[Bible en Dioula de Burkina Faso]\n"
msgstr ""
