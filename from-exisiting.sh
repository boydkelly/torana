#!/usr/bin/bash

ME="Boyd Kelly"
PACKAGE="Blog"
VERSION="1.0"
file=$1
localized=$2
#OPTIONS="-o tablecells"

[[ -z $file ]] && { echo need file name ; exit 1 ; } ; 
[[ ! -f $file ]] && { echo need master file ; exit 1 ; } ; 

# source translation language
source="en"
target="fr"

# root of the documentation repository
content="./content"

[[ -z "$POT_DIR" ]] && POT_DIR="./l10n/pot"
[[ -z "$PO_DIR" ]]	&& PO_DIR="./l10n/po"

# the base folder where your project content lives
[[ ! -d "$content" ]] &&  { echo "Error, please check that Scontent matches the root of the documentation repository" ; exit 1 ; }

basename=$(basename -s .adoc "$file")
dirname=$(dirname "$file")
path=$(dirname ${file#${content#./}/})
#minus the content base and file name: for example /fr/post
subdir=${path#$source}
#minus the language for example /post
pot_file="$POT_DIR/$source/$basename.adoc.pot"
po_file="$PO_DIR/$target/$basename.adoc.po"
localized="${file/\/$source/\/$target}"

[[ -f $pot_file ]]  && { echo "POT file exists already. Exitiing..." ; exit 1 ; }
[[ -f $po_file ]]  && { echo "PO file exists already. Exitiing..." ; exit 1 ; }

echo $file
echo po_file $po_file 
echo pot_file $pot_file 
echo localized $localized
      po4a-gettextize \
        --format asciidoc $OPTIONS \
        --master "$file" \
        --master-charset "UTF-8" \
        --localized-charset "UTF-8" \
        --copyright-holder "$ME" \
        --package-name "$PACKAGE" \
        --package-version "$VERSION" \
        --po "$pot_file" \
        --verbose 
              #--option debug split_attributelist -l content/en/post/anki.adoc -p l10n/po/en/post/anki.po

              echo Updated $pot_file
              sed -i '/X-Source-Language:.*/d' $pot_file &&  sed -i "/Content-Type:/a \"X-Source-Language: $source $(printf '%q' '\n')\"" $pot_file 
              echo Added source language to $pot_file

    if [ -f $localized ] ; then
      mkdir -p "$PO_DIR/$target/$subdir"
      #create po file from source
      po4a-gettextize \
        --format asciidoc $OPTIONS \
        --master "$file" \
        --localized "$localized" \
        --master-charset "UTF-8" \
        --localized-charset "UTF-8" \
        --copyright-holder "$ME" \
        --package-name "$PACKAGE" \
        --package-version "$VERSION" \
        --po "$po_file" \
        --verbose 
              #--option debug split_attributelist -l content/en/post/anki.adoc -p l10n/po/en/post/anki.po

              echo Updated $po_file
              sed -i '/X-Source-Language:.*/d' $po_file &&  sed -i "/Content-Type:/a \"X-Source-Language: $source $(printf '%q' '\n')\"" $po_file 
              sed -i "s/^\"Language:.*$/\"Language: $target $(printf '%q' '\n')\"/g" $po_file 
              echo Added source and target languages to $po_file
            else
              echo "No other previously translated document found in $target!"
    fi
git add $po_file $pot_file $localized -f
