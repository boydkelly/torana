build:
	po4a *.cfg
	git add -f l10n/pot/*/*.pot 
	git add -f l10n/po/*/*.po 

date := $(shell date -Ih)
publish:
	echo $(date)
	git commit --quiet -a -m "published on: $(date)"
	git push origin main --quiet

