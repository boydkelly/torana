---
title: "If the shoe fits..."
date: 2020-06-06T11:11:52Z
author: Molière 
draft: false
lang: en 
type: false
tags: ["proverb", "french"]
categories: ["English in French"]
---

= If the Shoe Fits 

[quote]
____
If the shoe fits, wear it.
____
