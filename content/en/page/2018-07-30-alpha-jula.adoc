---
title:  Jula Alphabet
author: Boyd Kelly
date:  2018-07-30T22:14:31Z
type: page
keywords: [ jula, julakan, dioula, dyula, dyu, international, internationale, keyboard, clavier, android, baoule, "ivory coast", "côte d'Ivoire", bete, senoufo, language, langues afrique, africa, technologie, technology, multilingual, multilingue, français, french, anglais, english, linux ]
image: /images/blackboard.jpg
tags: ["jula", "language"]
categories: ["Julakan"]
menu:
  jula:
---

= Jula Alphabet
:author: Boyd Kelly
:date: 2018-07-30T22:14:31Z
:description: Alphabet Jula
:type: page
:lang: en 
:image: /images/blackboard.jpg
:tags: ["jula", "language"]
:keywords: [ jula, julakan, dioula, dyula, dyu, international, internationale, keyboard, clavier, android, baoule, "ivory coast", "côte d'Ivoire", bete, senoufo, language, langues afrique, africa, technologie, technology, multilingual, multilingue, français, french, anglais, english, linux ]
:categories: ["Julakan"]
include::./locale/attributes.adoc[fr]


.Jula Alphabet
[width="100%", cols="4", frame="topbot", options="header,footer", stripe="even"]
|====
|  | As in english | Jula example | English meaning

| a
| apple
| saga
| sheep

| aa
| dark
| baara
| travail

| an
| blanc (fr)
| sanji
| pluie

| b
| big
| baga
| porridge

| c
| match
| man
| homme

| d
| dot
| disi
| chest

| e
| ray
| bese
| machete

| ee
| x
| feere
| sell

| en
|| sen
| foot

| ɛ
| enter
| sɛnɛ
| agriculture

| ɛɛ
|| mɛɛn
| last

| ɛn
|| bɛn
| agree

| f
| fox
| foro
| field

| g
| get
| mɔgɔ
| person

| gw
|| gwa
| foyer

| h
| hello (anglais)
| hakili
| intelligence

| i
| Iit
| misi
| boeuf

| ii
|| miiri
| pensée

| in
| I
| Min
| boire

| j
| Abidjan
| ji
| eau

| k
| kaolin
| kitabu
| livre

| l
| lame
| lolo
| étoile

| m
| main
| maro
| riz

| n
| nuage
| nɔnɔ
| lait

| ɲ
| oignon
| ɲɔ
| mil

| ŋ
| parking
| ŋɔni
| épine

| o
| mot
| bolo
| bras

| oo
|| looru
| cinq

| on
| don
| bon
| maison

| ɔ
| sort
| sɔrɔ
| trouver

| ɔɔ
|| fɔɔnɔ
| vomir

| ɔn
|| dɔn
| danser

| p
| pont
| pan
| sauter

| r
|| bɔrɔ
| sac

| s
| sauce
| sara
| payer

| t
| tête
| toto
| rat

| u
| loup
| kura
| nouveau

| uu
| football
| suuri
| baisser

| un
|| kunkolo
| tête

| v
| vol
| Vamara
| nom masculin

| w
| doigt
| wari
| argent

| y
| papaye
| yaala
| se promener

| z
| gaz
| zɔnzɔn
| crevette
|====
