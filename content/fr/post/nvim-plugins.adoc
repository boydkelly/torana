---
title: Nvim plugins
author: Boyd Kelly
date: 2020-07-14T08:36:57
type: post
draft: false
tags: ["tech", "vim", "neovim"]
categories: ["Technologie"]
---

= Nvim plugins 
:author: Boyd Kelly
:email: 
:date: 2020-05-07T08:36:57
:description:
:filename: nvim-plugins.adoc
:type: post 
:keywords: ["vim", "plugins"]
:tags: ["tech", "vim", "neovim"]
:categories: ["Technologie"]
:lang: fr 

.Nvim plugins
[width="100%",cols="5",format=csv,frame="topbot",options="header,footer",stripes="even"]
|====
Startup,Plugin,Version,Usage,Notes
include::{includedir}nvim-plugins.csv[]
|====
