---
title:  Yet Still Again ?
date:  2018-05-21 22:46:26 +0000
type: post
tags: ["langue", "français", "anglais"]
categories: ["Anglais en français"]
---

= Yet Still Again ?
:author: Boyd Kelly
:date: 2018-05-21 22:46:26 +0000
:description: Yet? Still? Again?
:type: post
:lang: fr 
include::./locale/attributes.adoc[fr]

== Quelle est la différence entre ces trois termes ?

[abstract]
Souvent le français est plus précis que l'anglais... Surtout en matière de verbes.
Par contre l'anglais peut aussi être plus précis que le français.

== Voici un exemple

Yet:: Encore
Still:: Encore
Again:: Encore 
Even:: Encore (souvent traduit par 'même', mais parfois 'encore')

et encore.footnote:[Dans link:https://www.coastsystems.net/en/post/ysa[la version anglaise] de ce document j'ai traduit 'encore' par 'aussi'.] : 

Yet again:: Encore

Oui, parfois on trouve en anglais les deux mots ensemble dans une phrase : '*yet again*...'
Qu'est-ce que cela signifie ?

Nous avons non seulement quatre mots en anglais, mais quatre significations différentes avec la même traduction en français.
Connaissez-vous la nuance?

.Voici quelques phrases pour vous aider:
****
She hasn't come yet:: Elle n'est pas encore venue
She is yet to come:: Elle n'est pas encore venue 
She is still sleeping::  Elle dort encore.  (Elle continue à dormir)
She is eating again::  Elle mange encore.  (Elle mange de nouveau)
She is going yet again::  Elle va encore.  (Elle continue d'y aller une fois de plus) 
It's even more difficult than I thought:: C'est encore plus difficile que je ne le pensais.
There's even more to come:: Il y en a encore davantage à venir.
****

[width="100%",cols="2,8",frame="topbot",options="none",stripe="even"]
|===
|Yet|Une action qui ne s'est pas produit jusqu'au présent. (encore)
|Still|Une action qui continue à se produire, qui n'a jamais arrêté.
|Again|Une action discontinue qui reprend.
|Even|Nuancé avec 'still'|Une chose qui s'est arrêtée/épuisée, mais il y en aura...|(encore !)
|Yet again|Une action discontinue qui reprend au moins une fois mais probablement plus.
|===


