---
title: Astuces pour éditer asciidoc avec neovim/Vim (comme un boss !) 
author: Boyd Kelly
date: 2021-01-02
type: post
draft: false
categories: ["Technologie"]
tags: ["vim", "asciidoc"]
---

= Astuces pour éditer asciidoc avec neovim/Vim (comme un boss !) 
:author: Boyd Kelly
:date: 2021-01-02
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:draft: false
:tags: ["vim", "asciidoc"]
:categories: ["Technologie"]
:lang: fr 
include::locale/attributes.adoc[]

Après avoir passé une année et plus à éditer en asciidoc, j'ai pensé partager quelques suggestions et astuces que j'ai trouvé pratique.

Signalons d'abord qu'il existe des centaines de paramètres et options en néovim/Vim pour le formatage et présentation du texte en générale. Parfois cette présentation peut être utile pour rendre la lecture agréable.
Par contre rappelons nous que la raison d'être de documents en asciidoc, c'est que la chaine d'outils va s'occuper de la présentation.
Heureusement, si on suit cette recommandation de "Une phrase par ligne", on peut bien mettre de côté la plupart de ces paramètres de 'formatage'.
Asciidoctor va faire son travail, et css, asciidoctor-pdf, votre serveur web et navigateur vont s'occuper de la présentation.

C'est intéressant que les suggestions dans le vieux document asciidoc https://asciidoc.org/chunked/ch36.html#X61[Tips and trickspour vim], suggère des paramètres pour formater les listes avec les numéros et caractères alphabétiques. Mais en asciidoc on n'a pas besoin de cela. Les listes asciidoc commence avec . * - etc.
Alors quels paramètres sont vraiment utiles ?

== Greffons recommandés

. https://github.com/habamax/vim-asciidoctor[Vim Asciidoctor]
** coloration syntaxique
** sections cachées
** lancer la chaine d'outils (création de fichiers html/pdf)

. https://github.com/SirVer/ultisnips[Ultisnips] 
** créer des extraits de texte que vous saisissez souvent

== Présentation et formatage du texte dans neovim/Vim :

Démarrons avec un paramètre pour gérer les autres :

[source,vimscript]
----
set formatoptions=tcqr
----

* *tc* signifie gérer le format de texte et commentaires dans le code. Ce formatage est *désactivé* tant que le paramètre textwidth=0, mais *sera activé* de façon manuel en utilisant le raccourci gq. (Voilà encore exactement ce que nous voulons ! voir en bas.)
* *q* active le raccourci gq pour formatage manuel du text avec formatexp.
* *r* signifie insérer automatiquement le(s) caractère(s) de commentaire après avoir appuyé sur kbd:[Enter] (Encore exactement ce que nous avons besoin, voir plus bas !)

Ensuite, parmi les plus importantes sont ces deux paramètres :

[source,vimscript]
----
textwidth=0
wrap
----

Cela empêche effectivement Vim de formater et insérer des 'retours dur' dans votre document. Le texte va 'couler', visiblement selon la largeur du terminal, mais pour Vim c'est toujours une seule ligne.
Mais pour que le texte soit plus lisible, et que vous 'voyez' ce qui se passe, ajoutez ce qui suit :

[source,vimscript]
----
setl linebreak  "ne pas couper les mots pour faire de nouvelle lignes"
----

Les trois paramètres suivants créent un 'retrait de paragraphe' pour chaque ligne après la première. A partir de la deuxième ligne Vim va indiquer les 'retours doux' avec une indentation de 2 caractères suivi par un ↳ et la continuation de la phrase. 

[source,vimsript]
----
setl showbreak = ↳
setl breakindent
setl breakindentopt = min:20,shift:2
----

== Texte déjà mal formaté/coupé

Parfois vous avez coupé-collé du texte ou bien cela vient d'un autre document formaté à 80 caractères par ligne. Vim au secours. Inclure une fonction qui serait 'attachée' à la commande de formatage manuel gq. Merci à https://vi.stackexchange.com/questions/28615/how-to-join-using-formatexpr-and-function/28620#28620[Vim and Vi Beta Stack Exchange] pour un peu d'aide avec cette fonction :

[source,vimscript]
----
function! OneSentencePerLine()
 if mode() =~# '^[iR]'
  return
 endif
 let start = v:lnum
 let end = start + v:count - 1
 execute start.','.end.'join'
 s/[.!?]\zs\s*\ze\S/\r/g
endfunction
set formatexpr=OneSentencePerLine()
----

Vous pouvez ainsi faire une sélection des paragraphes avec kbd:[v] kbd:[i] kbd:[p] et ensuite taper kbd:[g+q] pour instantanément transformer le texte en une ligne par phrase ! 

== Fonctionnalité de neovim/Vim

Le paramètre suivant permet de curseur d'agir comme dans un traitement de texte traditionnel avec les touches flèches, et les touches de déplacement Vim j et k. Effectivement ces touches au début et a la fin des lignes permettront de déplace le curseur sur la prochaine ligne ou bien ligne précédente le cas échéant

[source,vimscript]
----
setl whichwrap+=<,>,h,l,[,]
----

Quand il a a une 'soft wrap', sur l'écran, le déplacement 'normal' du curseur dans Vim serait de 'sauter' la ligne. Si vous voulez éditer au milieu de cette phrase coupée, cela peut être frustrant. Le paramètre suivant permet de déplacer le curseur 'normalement' à travers ces lignes avec un 'soft wrap'. (Voir https://www.reddit.com/r/vim/comments/82sqoc/any_caveats_to_binding_j_and_k_to_gj_and_gk/[cette discussion] pour voir les avantages/désavantages de ce changement.)

[source,vimscript]
----
nnoremap <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
nnoremap <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'
----

Maintenant comment faciliter la création de listes ? Comme déjà mentionné, il n'y a pas vraiment besoin de 'formater' les listes asciidoc d'une façon particulière dans Vim. Par contre un paramètre peut aider plutôt à les saisir. On utilise la fonctionnalité de 'commentaires' dans Vim pour répéter le caractère à début des lignes selon certaines critères.
Souvent cela peut être une préférence personnelle ou bien dépend du document. Donc les suggestions suivantes sont des idées qui peuvent s'adapter.

Le paramètre suivant peut bien servir de défaut pour les asciidocs. Si vous tapez un -, *, +, ., ., suivi par une espace et texte, Vim va commencer la ligne suivante avec le même caractère. (Pas besoin de formater des IV, a) etc) Dans le cas du '|', pas besoin d'espace pour déclencher le formatage automatique. 
Si quelqu'un a des suggestions ici, je serais ravis de les connaître.

[source,vimscript]
----
setl comments=://,b:#,:%,:XCOMM,b:-,b:*,b:.,:\|
----

Mais si vous devez saisir plein de listes au même format comme :

* Fruits
. Passion
. Ananas
. Mangoustan

[source,vimscript]
----
set comments=slb:*,mb-2:.,elx:\
----

ou encore :

* Pays avec l'anglais et le français comme langues officielles :
** Le Cameroun
** Le Canada

[source,vimscript]
----
set comments=sb:*,mb-2:**,elx:\
----

Il y a beaucoup de possibilités ici. :h comments est votre ami ! Si vous éditez dans une table, avec la première suggestion en haut la barre se placera automatiquement sur chaque ligne. 
Mais pour les options de table, je suggère le plugin Ultisnips (ou autre de votre choix) qui peut vraiment faciliter saisir les textes plus détaillés.

