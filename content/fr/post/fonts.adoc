---
title: Polices d'écriture adaptées pour le bambara, le jula (et d'autres langues mandingues)
date: 2020-10-21
type: post 
draft: false
tags: [ "julakan", "langue", "web", "unicode"]
categoriess: ["Julakan", "Technologie" ]
image: /images/polices.png 
hyperlink: https://www.coastsystems.net/dyu/form/ngalonci
---

= Polices d'écriture adaptées pour le bambara, le jula (et d'autres langues mandingues)
:author: Boyd Kelly
:email:
:date: 2020-10-21
:filename: fonts.adoc
:description: Polices pour bambara, jula (et d'autres langues mandenkan)
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", mandenkan, manden, mandingue, jula, julakan, dioula, polices, fonte, typographie, i18n, baoulé, bété, senoufo, senari, wawle, "ˈBhɛtɩgbʋʋ"]
:lang: fr 
include::locale/attributes.adoc[]

image::polices.png[role='left']

[abstract]
Noto Sans ka ɲi ani Nunito ma ɲi dɛ!

L'internet est international, mais ce n'est pas toutes les polices.footnote:[En typographie, une police d’écriture, ou police de caractères, est un ensemble de glyphes, c’est-à-dire de représentations visuelles de caractères d’une même famille, qui regroupe tous les corps et graisses d’une même famille, dont le style est coordonné, afin de former un alphabet, ou la représentation de l’ensemble des caractères d’un langage, complet et cohérent.], (fonts) qui sont adaptés aux langues africaines.
Pour bien afficher le text, vous aurez besoin de polices qui gèrent les caractères unicodes suivants:

[width="80%",cols="2",frame="topbot",options="header",stripe="even"]
|===
|Glyph IPA|Unicode
|ɔ	|"0254
|Ɔ	|"0186
|ɛ	|"025B
|Ɛ	|"0190	
|ɲ	|"0272
|Ɲ	|"019D 
|ŋ	|"014B
|Ŋ	|"014A
|===

// ɔ  0254 Ɔ  0186 ɛ  025B Ɛ  0190   ɲ  0272 Ɲ  019D  ŋ  014B Ŋ  014A

[WARNING]
Même des polices très répandues comme link:https://fonts.google.com/specimen/Roboto?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Roboto], (Google) ne gèrent pas les langues mandingues.

En fait, le navigateur substitue généralement une police d'éctiture système pour rendre des glyphes manquants.
Souvent cette substitution de glyphes d'une autre police système peut donner un résult acceptable ou même très bien.
Par contre pour ne jamais avoir de surprises dans la présentation de votre application ou page web, choisessz des polices adaptées à la langue !
La police web la plus populaire chez Google qui gère tous les glyphes IPA c'est Noto Sans ou bien Noto Serif.
Bien qu'on parle de langues mandingues ici, si votre text est en Sénari(Senoufo) ou bien Wawle(Baoulé) ou encore ˈBhɛtɩgbʋʋ (Bete), le choix de police devient encore plus important.

****
Voici un link:https://www.coastsystems.net/dyu/form/ngalonci[court texte] qui permet de faire la vérification d'une police de caractères web et voir si elle peut afficher les caractères propre aux langues mandingues.

Cherchez d"autres polices web sur... link:https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Google Fonts]
****
NOTE: Ce site web utilse link:https://fonts.google.com/specimen/Noto+Sans?preview.text=%C9%94%20%200254%20%C6%86%20%200186%20%C9%9B%20%20025B%20%C6%90%20%200190%20%20%20%C9%B2%20%200272%20%C6%9D%20%20019D%20%20%C5%8B%20%20014B%20%C5%8A%20%20014A&preview.text_type=custom&query=noto[Noto Sans] comme défault.

