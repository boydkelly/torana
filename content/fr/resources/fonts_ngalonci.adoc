---
title: Vérifier une polices pour Bambara, Jula (et d'autres langues mandenkan)
date:  2020-05-08T14:07:30
type: notification 
tags: [ "jula", "langue", "web"]
categories: ["Julakan" ]
image: /images/polices.png 
hyperlink: https://www.coastsystems.net/dyu/form/ngalonci
---

= Vérifier une polices pour Bambara, Jula (et d'autres langues mandenkan)
:author: Boyd Kelly
:email:
:date: 2020-10-21
:filename: polices.adoc
:description: Polices pour Bambara, Jula (et d'autres langues mandenkan)
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula, polices, fonts, i18n]
:lang: fr 
include::locale/attributes.adoc[]

L'internet est international, mais ce n'est pas toutes les polices, (fontes)
qui sont adaptées aux langues africaines.
link:https://www.coastsystems.net/dyu/ngalonci[Voici une page] qui permet de
faire la vérification d'une police de caractères web et voir si elle peut
afficher les caractères propre aux langues mandingues.

