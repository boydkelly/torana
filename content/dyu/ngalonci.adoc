---
title: Ngalonci
date: 2020-06-18
author: karamɔgɔ 
draft: false
type: page
lang: dyu
tags: ["julakan", "tariku"]
categories: ["tarikuw"]
image: /images/DSC00932.JPG
layout: ngalonci
menu:
  jula:
---

= Ngalonci
:author: inconnu
:date: 2020-06-18
:type: post
:lang: dyu
:imagesdir: /images/
:skip-front-matter:

image:DSC00932.JPG[role="left"]

Cɛ dɔ tun bɛ dugu dɔ la, o cɛ tɔgɔ tun kiyanin kojugu. A tolo don na masacɛ dɔ ya nafolotigiya la. A ka a fɔ a muso ɲana ko a ye a torolasanu d’a ma. A facɛ fana sa la ka so kelen to a bolo. A ka fɔ a a muso ɲana ko a bɛ taga masacɛ fɛ ka taga a ya masaya bɔsi a la. A ka ɲɔ kɛ so kun ganfa la ka sanukuru bla o la. So ka ɲɔ ni sanu kunu. A sɔrɔ la ka taga fɔ masacɛ ɲana ko so dɔ bɛ ale bolo. So bo ye sanu ye. So ka bo kɛ, a ka bo nin ko ka sanu ye a la.



O kɛnin, masacɛ ka so san sɔngɔ gbɛlɛn na. A ka so bla kuru dɔ kɔnɔ, ni so ka bo min kɛ, o bɛ o cɛ fɔ so nan na bo caman kɛ. O ka sɔrɔ cɛ se la yɔrɔ jan. Masacɛ ka a lase jɔnmuso bɛɛ ma ko a ya so boko ye bi ye. Musow nan na ni fiyɛw ye ka nan bo bɛɛ ko sanukuru kelen ne sɔrɔ la a la. Masacɛ ko: « A ye taga cɛ nin wele ka nan n b’a kan tigɛ ». Cɛ nantɔ, a ni a muso bɛn na a la ko ni a ni masacɛ ka kuman daminɛ, i bɛ n sɔsɔ ka n galontigiya. O tuman na a ka sisɛ faga ka sisɛ joli kɛ sisɛjogi kɔnɔ ka a siri a muso kankɔrɔ.





